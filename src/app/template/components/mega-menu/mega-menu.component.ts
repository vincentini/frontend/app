import { Component, OnDestroy, OnInit } from '@angular/core';
import { PermissionsService } from '../../../security/services/permissions.service';
import { MenuGroup } from '../../../models/security/menu.model';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-mega-menu',
    templateUrl: './mega-menu.component.html',
    styleUrls: [ './mega-menu.component.scss' ]
})
export class MegaMenuComponent implements OnInit, OnDestroy {

    menuGroups: MenuGroup[];
    subscriptions: Subscription[] = [];

    constructor(private permissionsService: PermissionsService) {
    }

    ngOnInit(): void {
        const s1 = this.permissionsService.getUserMenus()
            .subscribe(menuGroups => this.menuGroups = menuGroups);

        this.subscriptions.push(s1);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }
}
