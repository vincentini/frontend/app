import { Component, OnDestroy, OnInit } from '@angular/core';
import { PermissionsService } from '../../../security/services/permissions.service';
import { MenuGroup } from '../../../models/security/menu.model';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-right-side',
    templateUrl: './right-side.component.html',
    styleUrls: [ './right-side.component.scss' ]
})
export class RightSideComponent implements OnInit, OnDestroy {

    menuGroups: MenuGroup[];
    subscriptions: Subscription[] = [];

    constructor(private permissionsService: PermissionsService) {
    }

    ngOnInit(): void {
        const s1 = this.permissionsService.getUserMenus()
            .subscribe(menuGroups => this.menuGroups = menuGroups);

        this.subscriptions.push(s1);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

}
