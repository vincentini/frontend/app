import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-form-toolbar',
    templateUrl: './form-toolbar.component.html',
    styleUrls: [ './form-toolbar.component.scss' ]
})
export class FormToolbarComponent implements OnInit {

    @Input() moduleName = '';
    @Input() menu = '';
    @Input() subMenu = '';

    constructor() {
    }

    ngOnInit(): void {
    }

}
