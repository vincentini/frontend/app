import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../../security/services/auth.service';
import { User } from '../../../models/security/user.model';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: [ './header.component.scss' ]
})
export class HeaderComponent implements OnInit, OnDestroy {

    user: User = {};

    subscriptions: Subscription[] = [];

    constructor(private authService: AuthService) {
    }

    ngOnInit(): void {
        const s1 = this.authService.getUserInfo()
            .subscribe(user => this.user = user);

        this.subscriptions.push(s1);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }
}
