import {NgModule} from '@angular/core';
import {PluginsModule} from '../plugins/plugins.module';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {RightSideComponent} from './components/right-side/right-side.component';
import {RouterModule} from '@angular/router';
import { FormToolbarComponent } from './components/form-toolbar/form-toolbar.component';
import { MegaMenuComponent } from './components/mega-menu/mega-menu.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    RightSideComponent,
    FormToolbarComponent,
    MegaMenuComponent
  ],
  imports: [
    PluginsModule,
    RouterModule
  ],
    exports: [
        HeaderComponent,
        FooterComponent,
        RightSideComponent,
        FormToolbarComponent
    ]
})
export class TemplateModule {
}
