import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Utils } from '../utils/utils';

@Injectable({
  providedIn: 'root'
})
export class NavigationEventsService {

  constructor(private router: Router) {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        // Utils.init();
      }
    })
  }
}
