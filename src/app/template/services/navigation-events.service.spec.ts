import { TestBed } from '@angular/core/testing';

import { NavigationEventsService } from './navigation-events.service';

describe('NavigationEventsService', () => {
  let service: NavigationEventsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NavigationEventsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
