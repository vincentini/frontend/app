import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CrudService } from '../../../general/services/crud.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../plugins/services/alert.service';
import { Table } from '../../../models/metadata/table';
import { fieldTypes } from '../../../models/metadata/field';
import { combineLatest } from 'rxjs';

@Component({
    selector: 'app-table-form',
    templateUrl: './table-form.component.html',
    styleUrls: [ './table-form.component.scss' ]
})
export class TableFormComponent implements OnInit {

    table: Table = {};

    _fieldTypes = fieldTypes;

    errors = {
        table_name: undefined,
        table_description: undefined
    };

    @ViewChild('nameField') nameField: ElementRef;

    constructor(private crudService: CrudService,
                private router: Router,
                private route: ActivatedRoute,
                private alertService: AlertService) {
    }

    ngOnInit(): void {
        const url = this.route.snapshot.url.join('');

        if (url !== 'new') {
            const s1 = this.route.params.subscribe(params => {
                if (params.id) {
                    this.crudService
                        .get<Table>('metadata/tables', params.id)
                        .subscribe(table => this.table = table, errors => this.errors = errors);
                }
            });
        }
    }

    ngAfterViewInit(): void {
        this.nameField.nativeElement.focus();
    }

    addColumn() {
        this.table.table_fields = this.table.table_fields || [];
        this.table.table_fields.push({ field_type: 1, field_size: 255 });
    }

    save(): void {
        // @ts-ignore
        this.errors = {};

        if (this.isNew()) {
            this.crudService.create('metadata/tables', this.table)
                .subscribe(result => {
                    let subscriptions = this.table.table_fields.map(f => this.crudService.create('metadata/fields', { table: result.id, ...f }));

                    combineLatest(subscriptions)
                        .subscribe(results => {
                            this.sucess('incluído');
                        }, error => {
                            this.errors = error;
                            this.crudService.delete('metadata/tables', result.id).subscribe();
                        });


                }, errors => this.errors = errors);
        } else if (this.isEdit()) {
            this.crudService.update('metadata/tables', this.table.id, this.table)
                .subscribe(result => {
                    let subscriptions = this.table.table_fields.map(f => {
                        if (f.id) {
                            return this.crudService.update('metadata/fields', f.id, { ...f });
                        }
                        return this.crudService.create('metadata/fields', { table: result.id, ...f });
                    });

                    combineLatest(subscriptions)
                        .subscribe(results => {
                            this.sucess('alterado');
                        }, error => {
                            this.errors = error;
                        });
                }, errors => this.errors = errors);
        }
    }

    sucess(acao: string): void {
        this.alertService
            .success(`Registro ${ acao }  com sucesso!`)
            .then(() => this.cancel());
    }

    cancel(): void {
        this.router.navigate([ this.route.parent.parent.routeConfig.path ]);
    }

    isNew(): boolean {
        const url = this.route.snapshot.url.join('/');
        return url === 'new';
    }

    isEdit(): boolean {
        const url = this.route.snapshot.url.join('/');
        return url.startsWith('edit');
    }
}
