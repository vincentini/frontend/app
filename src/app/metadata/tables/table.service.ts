import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CustomerFileData } from '../../models/basic-tables/customer-file-data';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor(private httpClient: HttpClient) { }

  createTable(id: number) {
    return this.httpClient
        .post<void>(`${ environment.baseApiUrl }/api/metadata/tables/${ id }/create-table/`, undefined);
  }

  dropTable(id: number) {
    return this.httpClient
        .post<void>(`${ environment.baseApiUrl }/api/metadata/tables/${ id }/drop-table/`, undefined);
  }
}
