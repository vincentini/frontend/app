import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PageResult } from '../../../models/list/page-result.model';
import { CrudService } from '../../../general/services/crud.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../plugins/services/alert.service';
import { DataPaginationEvent } from '../../../plugins/components/data-table/model/data-pagination.event';
import { TableService } from '../table.service';
import Table = WebAssembly.Table;

@Component({
    selector: 'app-table-list',
    templateUrl: './table-list.component.html',
    styleUrls: [ './table-list.component.scss' ]
})
export class TableListComponent implements OnInit {


    searchBy = '';
    pageSize = 10;
    data: PageResult<Table> = {};
    @ViewChild('searchField') searchField: ElementRef;


    constructor(
        private crudService: CrudService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private alertService: AlertService,
        private tableService: TableService) {
    }

    ngOnInit(): void {
        this.search();
    }

    ngAfterViewInit(): void {
        this.searchField.nativeElement.focus();
    }

    onPaginate(event: DataPaginationEvent): void {
        this.search(event.page);
    }

    search(pageNumber = 1): void {
        this.crudService
            .list<Table>('metadata/tables', this.searchBy, pageNumber, this.pageSize)
            .subscribe(result => this.data = result);
    }

    newTable(): void {
        this.router.navigate([ 'new' ], { relativeTo: this.activatedRoute });
    }

    editTable(id: number): void {
        this.router.navigate([ 'edit', id ], { relativeTo: this.activatedRoute });
    }

    deleteTable(id: number) {
        this.alertService.confirmSimNao('Confirma a EXCLUSÃO deste cliente?')
            .then((result) =>
                result.isConfirmed ? this.confirmDelete(id) : undefined);
    }

    createTable(id: number) {
        this.tableService.createTable(id)
            .subscribe(() => this.alertService.success('Tabela criada com sucesso!'),
                error => this.alertService.error(`Ocorreu um erro ao criar a tabela! ${ error }`));

    }

    dropTable(id: number) {
        this.tableService.dropTable(id)
            .subscribe(() => this.alertService.success('Tabela apagada com sucesso!'),
                error => this.alertService.error(`Ocorreu um erro ao apagar a tabela! ${ error }`));

    }

    confirmDelete(id: number): void {
        this.crudService.delete('metadata/tables', id)
            .subscribe(() => {
                this.search(1);
                this.alertService.info('Registro excluído com sucesso!');
            }, (error => {
                this.alertService.error('Ocorreu um erro ao excluir o registro!');
            }));
    }


}
