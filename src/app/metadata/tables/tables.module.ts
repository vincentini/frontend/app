import { NgModule } from '@angular/core';

import { TablesRoutingModule } from './tables-routing.module';
import { TableComponent } from './table/table.component';
import { TableFormComponent } from './table-form/table-form.component';
import { TableListComponent } from './table-list/table-list.component';
import { PluginsModule } from '../../plugins/plugins.module';
import { GeneralModule } from '../../general/general.module';
import { TemplateModule } from '../../template/template.module';


@NgModule({
    declarations: [ TableComponent, TableFormComponent, TableListComponent ],
    imports: [
        PluginsModule,
        GeneralModule,
        TemplateModule,
        TablesRoutingModule
    ]
})
export class TablesModule {
}
