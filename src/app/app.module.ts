import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SecurityModule } from './security/security.module';
import { GeneralModule } from './general/general.module';
import { TemplateModule } from './template/template.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptorService } from './security/services/auth-interceptor.service';
import { CustomersModule } from './basic-tables/customers/customers.module';
import { FileCategoriesModule } from './basic-tables/file-categories/file-categories.module';
import { S3BucketsModule } from './basic-tables/s3-buckets/s3-buckets.module';
import { CustomerFilesModule } from './basic-tables/customer-files/customer-files.module';
import { TablesModule } from './metadata/tables/tables.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        SecurityModule,
        GeneralModule,
        TemplateModule,
        CustomersModule,
        CustomerFilesModule,
        FileCategoriesModule,
        S3BucketsModule,
        TablesModule
    ],
    exports: [],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptorService,
            multi: true
        }
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule {
}
