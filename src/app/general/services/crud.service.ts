import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import { PageResult } from '../../models/list/page-result.model';
import { catchError, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class CrudService {

    constructor(private httpClient: HttpClient) {
    }

    list<T>(entity: string, search: string, pageNumber: number, pageSize: number): Observable<PageResult<T>> {
        return this.httpClient
            .get<PageResult<T>>(`${ environment.baseApiUrl }/api/${ entity }/?search=${ search }&page=${ pageNumber }&page_size=${ pageSize }`)
            .pipe(
                map(result => {
                    const pages = (Math.trunc(result.count / pageSize)) + (result.count % pageSize > 0 ? 1 : 0);
                    return { ...result, pages };
                })
            );
    }

    get<T>(entity: string, id: any): Observable<T> {
        return this.httpClient
            .get<T>(`${ environment.baseApiUrl }/api/${ entity }/${ id }`);
    }

    create<T>(entity: string, data: T): Observable<T> {
        return this.httpClient
            .post<T>(`${ environment.baseApiUrl }/api/${ entity }/`, data)
            .pipe(
                catchError(((err, caught) => {
                    if (err.status === 400) {
                        return throwError(err.error);
                    }
                    return throwError(err);
                }))
            );
    }

    update<T>(entity: string, id: any, data: T): Observable<T> {
        return this.httpClient
            .patch<T>(`${ environment.baseApiUrl }/api/${ entity }/${ id }/`, data)
            .pipe(
                catchError(((err, caught) => {
                    if (err.status === 400) {
                        return throwError(err.error);
                    }
                    return throwError(err);
                })),
            );
    }

    delete(entity: string, id: any): Observable<any> {
        return this.httpClient
            .delete(`${ environment.baseApiUrl }/api/${ entity }/${ id }/`)
            .pipe(
                catchError(((err, caught) => {
                    if (err.status === 400) {
                        return throwError(err.error);
                    }
                    return throwError(err);
                })),
            );
    }
}
