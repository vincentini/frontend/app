import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AuthGuardService } from '../security/services/auth-guard.service';

const routes: Routes = [
    { path: 'home', component: HomeComponent, canActivate: [ AuthGuardService ] }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class GeneralRoutingModule {
}
