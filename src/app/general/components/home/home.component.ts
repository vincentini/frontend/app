import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Utils } from '../../../template/utils/utils';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: [ './home.component.scss' ]
})
export class HomeComponent implements OnInit, AfterViewInit {

    constructor() {
    }

    ngOnInit(): void {
    }

    ngAfterViewInit(): void {
        Utils.init();
    }
}
