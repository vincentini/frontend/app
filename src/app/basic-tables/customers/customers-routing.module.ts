import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomersComponent } from './customers/customers.component';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { CustomersFormComponent } from './customers-form/customers-form.component';

const routes: Routes = [
    {
        path: 'basic-tables/customers', children: [
            {
                path: '', component: CustomersComponent, children: [
                    { path: '', component: CustomersListComponent },
                    { path: 'new', component: CustomersFormComponent },
                    { path: 'edit/:id', component: CustomersFormComponent },
                ]
            }
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class CustomersRoutingModule {
}
