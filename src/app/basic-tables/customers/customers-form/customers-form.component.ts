import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Customer } from '../../../models/basic-tables/customer';
import { CrudService } from '../../../general/services/crud.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../plugins/services/alert.service';

@Component({
    selector: 'app-customers-form',
    templateUrl: './customers-form.component.html',
    styleUrls: [ './customers-form.component.scss' ]
})
export class CustomersFormComponent implements OnInit, AfterViewInit {

    customer: Customer = {};
    errors = {
        name: undefined,
        cnpj: undefined
    };
    @ViewChild('nameField') nameField: ElementRef;

    constructor(private crudService: CrudService,
                private router: Router,
                private route: ActivatedRoute,
                private alertService: AlertService) {
    }

    ngOnInit(): void {
        const url = this.route.snapshot.url.join('');

        if (url !== 'new') {
            const s1 = this.route.params.subscribe(params => {
                if (params.id) {
                    this.crudService
                        .get<Customer>('customers', params.id)
                        .subscribe(customer => this.customer = customer, errors => this.errors = errors);
                }
            });
        }
    }

    ngAfterViewInit(): void {
        this.nameField.nativeElement.focus();
    }

    save(): void {
        const url = this.route.snapshot.url.join('/');
        // @ts-ignore
        this.errors = {};

        if (url === 'new') {
            this.crudService.create('customers', this.customer)
                .subscribe(result => this.sucess('incluído'), errors => this.errors = errors);
        } else if (url.startsWith('edit')) {
            this.crudService.update('customers', this.customer.id, this.customer)
                .subscribe(result => this.sucess('alterado'), errors => this.errors = errors);
        }
    }

    sucess(acao: string): void {
        this.alertService
            .success(`Registro ${ acao }  com sucesso!`)
            .then(() => this.cancel());
    }

    cancel(): void {
        this.router.navigate([ this.route.parent.parent.routeConfig.path ]);
    }
}
