import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CrudService } from '../../../general/services/crud.service';
import { Customer } from '../../../models/basic-tables/customer';
import { PageResult } from '../../../models/list/page-result.model';
import { ActivatedRoute, Router } from '@angular/router';
import { DataPaginationEvent } from '../../../plugins/components/data-table/model/data-pagination.event';
import { AlertService } from '../../../plugins/services/alert.service';

@Component({
    selector: 'app-customers-list',
    templateUrl: './customers-list.component.html',
    styleUrls: [ './customers-list.component.scss' ]
})
export class CustomersListComponent implements OnInit, AfterViewInit {

    searchBy = '';
    pageSize = 10;
    data: PageResult<Customer> = {};
    @ViewChild('searchField') searchField: ElementRef;

    constructor(
        private crudService: CrudService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private alertService: AlertService) {
    }

    ngOnInit(): void {
        this.search();
    }

    ngAfterViewInit(): void {
        this.searchField.nativeElement.focus();
    }

    onPaginate(event: DataPaginationEvent): void {
        this.search(event.page);
    }

    search(pageNumber = 1): void {
        this.crudService
            .list<Customer>('customers', this.searchBy, pageNumber, this.pageSize)
            .subscribe(result => this.data = result);
    }

    newCustomer(): void {
        this.router.navigate([ 'new' ], { relativeTo: this.activatedRoute });
    }

    editCustomer(id: number): void {
        this.router.navigate([ 'edit', id ], { relativeTo: this.activatedRoute });
    }

    deleteCustomer(id: number) {
        this.alertService.confirmSimNao('Confirma a EXCLUSÃO deste cliente?')
            .then((result) =>
                result.isConfirmed ? this.confirmDelete(id) : undefined);
    }

    confirmDelete(id: number): void {
        this.crudService.delete('customers', id)
            .subscribe(() => {
                this.search(1);
                this.alertService.info('Registro excluído com sucesso!');
            }, (error => {
                this.alertService.error('Ocorreu um erro ao excluir o registro!');
            }));
    }

    viewCustomerFiles(id: number): void {
        this.router.navigate([ 'basic-tables', 'customer-files', id ]);
    }
}
