import { NgModule } from '@angular/core';
import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersComponent } from './customers/customers.component';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { CustomersFormComponent } from './customers-form/customers-form.component';
import { PluginsModule } from '../../plugins/plugins.module';
import { TemplateModule } from '../../template/template.module';

@NgModule({
    imports: [
        PluginsModule,
        CustomersRoutingModule,
        TemplateModule
    ],
    declarations: [ CustomersComponent, CustomersListComponent, CustomersFormComponent ],
    exports: [ CustomersComponent, CustomersListComponent, CustomersFormComponent ]
})
export class CustomersModule {
}
