import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { S3BucketsComponent } from './s3-buckets/s3-buckets.component';
import { S3BucketsListComponent } from './s3-buckets-list/s3-buckets-list.component';
import { S3BucketsFormComponent } from './s3-buckets-form/s3-buckets-form.component';


const routes: Routes = [
    {
        path: 'basic-tables/s3-buckets', children: [
            {
                path: '', component: S3BucketsComponent, children: [
                    { path: '', component: S3BucketsListComponent },
                    { path: 'new', component: S3BucketsFormComponent },
                    { path: 'edit/:id', component: S3BucketsFormComponent },
                ]
            }
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class S3BucketsRoutingModule {
}
