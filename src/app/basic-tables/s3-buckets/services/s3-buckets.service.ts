import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { catchError, take } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class S3BucketsService {

  constructor(private httpClient: HttpClient) { }

  createS3Bucket(id: number) {
    return this.httpClient
        .post(`${ environment.baseApiUrl }/api/s3-buckets/${id}/create-bucket/`, undefined)
        .pipe(
            catchError(((err, caught) => {
              if (err.status === 400) {
                return throwError(err.error);
              }
              return caught;
            }))
        );
  }
}
