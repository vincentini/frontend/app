import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { S3BucketsComponent } from './s3-buckets.component';

describe('S3BucketsComponent', () => {
  let component: S3BucketsComponent;
  let fixture: ComponentFixture<S3BucketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ S3BucketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(S3BucketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
