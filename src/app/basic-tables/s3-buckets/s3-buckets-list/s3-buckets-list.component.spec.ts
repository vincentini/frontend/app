import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { S3BucketsListComponent } from './s3-buckets-list.component';

describe('S3BucketsListComponent', () => {
  let component: S3BucketsListComponent;
  let fixture: ComponentFixture<S3BucketsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ S3BucketsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(S3BucketsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
