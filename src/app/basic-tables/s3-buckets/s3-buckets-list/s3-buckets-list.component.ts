import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PageResult } from '../../../models/list/page-result.model';
import { CrudService } from '../../../general/services/crud.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../plugins/services/alert.service';
import { DataPaginationEvent } from '../../../plugins/components/data-table/model/data-pagination.event';
import { take } from 'rxjs/operators';
import { S3Bucket } from '../../../models/basic-tables/s3-bucket';
import { S3BucketsService } from '../services/s3-buckets.service';
import { BlockUiService } from '../../../plugins/services/block-ui.service';

@Component({
    selector: 'app-s3-buckets-list',
    templateUrl: './s3-buckets-list.component.html',
    styleUrls: [ './s3-buckets-list.component.scss' ]
})
export class S3BucketsListComponent implements OnInit {

    searchBy = '';
    pageSize = 5;
    currentPage = 1;
    data: PageResult<S3Bucket> = {};
    @ViewChild('searchField') searchField: ElementRef;

    constructor(
        private crudService: CrudService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private alertService: AlertService,
        private s3BucketsService: S3BucketsService,
        private blockUiService: BlockUiService) {
    }

    ngOnInit(): void {
        this.search();
    }

    ngAfterViewInit(): void {
        this.searchField.nativeElement.focus();
    }

    onPaginate(event: DataPaginationEvent): void {
        this.search(event.page);
    }

    search(pageNumber = 1): void {
        this.currentPage = pageNumber;
        this.crudService
            .list<S3Bucket>('s3-buckets', this.searchBy, this.currentPage, this.pageSize)
            .subscribe(result => this.data = result);
    }

    newS3Bucket(): void {
        this.router.navigate([ 'new' ], { relativeTo: this.activatedRoute });
    }

    editS3Bucket(id: number): void {
        this.router.navigate([ 'edit', id ], { relativeTo: this.activatedRoute });
    }

    deleteS3Bucket(id: number) {
        this.alertService.confirmSimNao('Confirma a EXCLUSÃO deste bucket?')
            .then((result) =>
                result.isConfirmed ? this.confirmDelete(id) : undefined);
    }

    confirmDelete(id: number): void {
        this.blockUiService.block();
        this.crudService.delete('s3-buckets', id)
            .subscribe(() => {
                this.blockUiService.unBlock();
                this.search(1);
                this.alertService.info('Registro excluído com sucesso!');
            }, (error => {
                this.blockUiService.unBlock();
                this.alertService.error('Ocorreu um erro ao excluir o registro!');
            }));
    }

    createAwsS3Bucket(id: number): void {
        this.alertService
            .confirmSimNao('Esta ação irá <strong>CRIAR</strong> o bucket na amazon. Esta ação não poderá ser desfeita. <br/> Deseja continuar?')
            .then((result) =>
                result.isConfirmed ? this.confirmCreateAwsS3Bucket(id) : undefined);
    }

    confirmCreateAwsS3Bucket(id: number): void {
        this.blockUiService.block();
        this.s3BucketsService.createS3Bucket(id)
            .subscribe(() => {
                this.blockUiService.unBlock();
                this.alertService.success('Bucket criado com sucesso!');
                this.search(this.currentPage);
            }, (error) => {
                this.blockUiService.unBlock();
                this.alertService.error(`Ocorreu um erro ao criar o bucket: ${ error.message }`);
            });
    }
}
