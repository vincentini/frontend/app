import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { S3BucketsFormComponent } from './s3-buckets-form.component';

describe('S3BucketsFormComponent', () => {
  let component: S3BucketsFormComponent;
  let fixture: ComponentFixture<S3BucketsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ S3BucketsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(S3BucketsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
