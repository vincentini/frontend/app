import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CrudService } from '../../../general/services/crud.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../plugins/services/alert.service';
import { take } from 'rxjs/operators';
import { S3Bucket } from '../../../models/basic-tables/s3-bucket';

@Component({
  selector: 'app-s3-buckets-form',
  templateUrl: './s3-buckets-form.component.html',
  styleUrls: ['./s3-buckets-form.component.scss']
})
export class S3BucketsFormComponent implements OnInit {

  s3Bucket: S3Bucket = {

  };
  errors = {
    name: undefined,
    bucket_name: undefined
  };

  @ViewChild('nameField') nameField: ElementRef;

  constructor(private crudService: CrudService,
              private router: Router,
              private route: ActivatedRoute,
              private alertService: AlertService) {
  }

  ngOnInit(): void {
    const url = this.route.snapshot.url.join('');

    if (url !== 'new') {
      const s1 = this.route.params.subscribe(params => {
        if (params.id) {
          this.crudService
              .get<S3Bucket>('s3-buckets', params.id)
              .subscribe(s3Bucket => this.s3Bucket = s3Bucket, errors => this.errors = errors);
        }
      });
    }
  }

  ngAfterViewInit():void {
    this.nameField.nativeElement.focus();
  }

  save(): void {
    const url = this.route.snapshot.url.join('/');
    // @ts-ignore
    this.errors = {};

    if (url === 'new') {
      let s3BucketCreate = {...this.s3Bucket};

      delete s3BucketCreate.created

      this.crudService.create('s3-buckets', s3BucketCreate)
          .subscribe(result => this.sucess('incluído'), errors => this.errors = errors);
    } else if (url.startsWith('edit')) {
      let s3BucketUpdate = {...this.s3Bucket};

      if (s3BucketUpdate.created) {
        delete s3BucketUpdate.bucket_name
      }

      delete s3BucketUpdate.created

      this.crudService.update('s3-buckets', this.s3Bucket.id, s3BucketUpdate)
          .subscribe(result => this.sucess('alterado'), errors => this.errors = errors);
    }
  }

  sucess(acao: string): void {
    this.alertService
        .success(`Registro ${ acao }  com sucesso!`)
        .then(() => this.cancel());
  }

  cancel(): void {
    this.router.navigate([ this.route.parent.parent.routeConfig.path ]);
  }

}
