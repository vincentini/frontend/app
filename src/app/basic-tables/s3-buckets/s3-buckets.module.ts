import { NgModule } from '@angular/core';

import { S3BucketsRoutingModule } from './s3-buckets-routing.module';
import { S3BucketsComponent } from './s3-buckets/s3-buckets.component';
import { S3BucketsFormComponent } from './s3-buckets-form/s3-buckets-form.component';
import { S3BucketsListComponent } from './s3-buckets-list/s3-buckets-list.component';
import { TemplateModule } from '../../template/template.module';
import { PluginsModule } from '../../plugins/plugins.module';


@NgModule({
    declarations: [ S3BucketsComponent, S3BucketsFormComponent, S3BucketsListComponent ],
    imports: [
        PluginsModule,
        S3BucketsRoutingModule,
        TemplateModule
    ]
})
export class S3BucketsModule {
}
