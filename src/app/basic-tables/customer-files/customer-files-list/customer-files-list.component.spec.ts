import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerFilesListComponent } from './customer-files-list.component';

describe('CustomerFilesListComponent', () => {
  let component: CustomerFilesListComponent;
  let fixture: ComponentFixture<CustomerFilesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerFilesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerFilesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
