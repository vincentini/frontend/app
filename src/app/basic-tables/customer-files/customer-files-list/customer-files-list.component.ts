import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PageResult } from '../../../models/list/page-result.model';
import { Customer } from '../../../models/basic-tables/customer';
import { CrudService } from '../../../general/services/crud.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../plugins/services/alert.service';
import { DataPaginationEvent } from '../../../plugins/components/data-table/model/data-pagination.event';
import { CustomerFile } from '../../../models/basic-tables/customer-file';
import { BlockUiService } from '../../../plugins/services/block-ui.service';
import { CustomerFilesService } from '../services/customer-files.service';
import { Utils } from '../../../template/utils/utils';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { CustomerFileData } from '../../../models/basic-tables/customer-file-data';

@Component({
    selector: 'app-customer-files-list',
    templateUrl: './customer-files-list.component.html',
    styleUrls: [ './customer-files-list.component.scss' ]
})
export class CustomerFilesListComponent implements OnInit {

    searchBy = '';
    pageSize = 10;
    data: PageResult<Customer> = {};
    @ViewChild('searchField') searchField: ElementRef;
    @ViewChild('modalProcess') modalProcess;
    modalRef: BsModalRef;
    selectedCustomerFile: CustomerFile;
    selectedCustomerFileData: CustomerFileData = {};

    constructor(
        private crudService: CrudService,
        private customerFilesService: CustomerFilesService,
        private activatedRoute: ActivatedRoute,
        private alertService: AlertService,
        private blockUiService: BlockUiService,
        private modalService: BsModalService,
        private route: ActivatedRoute,
        private router: Router) {
    }

    ngOnInit(): void {
        this.search();
    }

    ngAfterViewInit(): void {
        this.searchField.nativeElement.focus();
    }

    onPaginate(event: DataPaginationEvent): void {
        this.search(event.page);
    }

    search(pageNumber = 1): void {
        this.blockUiService.block('.table');
        this.crudService
            .list<CustomerFile>('customer-files', this.searchBy, pageNumber, this.pageSize)
            .subscribe(result => {
                this.data = result;
                Utils.init();
                this.blockUiService.unBlock('.table');
            });
    }

    newCustomerFile(): void {
        this.router.navigate([ 'new' ], { relativeTo: this.activatedRoute });
    }

    editCustomerFile(id: number): void {
        this.router.navigate([ 'edit', id ], { relativeTo: this.activatedRoute });
    }

    deleteCustomerFile(id: number) {
        this.alertService.confirmSimNao('Confirma a EXCLUSÃO deste arquivo?')
            .then((result) =>
                result.isConfirmed ? this.confirmDelete(id) : undefined);
    }

    downloadCustomerFile(id: number): void {
        this.customerFilesService.getS3Link(id)
            .subscribe(link => window.open(link, '_blank'));
    }

    confirmDelete(id: number): void {
        this.blockUiService.block();
        this.crudService.delete('customer-files', id)
            .subscribe(() => {
                this.blockUiService.unBlock();
                this.search(1);
                this.alertService.info('Registro excluído com sucesso!');
            }, (error => {
                this.alertService.error('Ocorreu um erro ao excluir o registro!' + error.message);
                this.blockUiService.unBlock();
            }));
    }

    viewUpdateStatus(customerFile: CustomerFile): void {
        this.selectedCustomerFile = customerFile;

        this.customerFilesService
            .getAnalysisFromCustomerFile(this.selectedCustomerFile.id)
            .subscribe(r => {
                if (r.results.length > 0) {
                    this.selectedCustomerFileData = r.results[ 0 ];
                    this.showProgressModal();
                    this.updateStatusAnalysis();
                } else {
                    this.alertService.warn(`Nenhum resultado de processamento encontrado para o arquivo ${ this.selectedCustomerFile.file_name }!`)
                        .then(() => undefined);
                }
            });
    }

    goBack() {
        this.router.navigate([ 'basic-tables', 'customers' ]);
    }

    private showProgressModal() {
        this.modalRef = this.modalService.show(this.modalProcess,
            { animated: true, ignoreBackdropClick: true, class: 'modal-dialog-centered' });
    }

    private updateStatusAnalysis() {
        this.crudService
            .get('customer-file-data', this.selectedCustomerFileData.id)
            .subscribe((data) => {
                this.selectedCustomerFileData = data;

                if (this.selectedCustomerFileData.status !== 'FINISHED') {
                    setTimeout(() => this.updateStatusAnalysis(), 3000);
                }
            });
    }

    private showResults(customerFileDataId: number) {
        if (this.modalRef) {
            this.modalRef.hide();
        }
        this.router.navigate([ 'results', customerFileDataId ], { relativeTo: this.activatedRoute });
    }
}
