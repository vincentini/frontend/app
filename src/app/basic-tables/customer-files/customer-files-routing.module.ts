import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerFilesComponent } from './customer-files/customer-files.component';
import { CustomerFilesListComponent } from './customer-files-list/customer-files-list.component';
import { CustomerFilesUploadComponent } from './customer-files-upload/customer-files-upload.component';
import { CustomerFilesFormComponent } from './customer-files-form/customer-files-form.component';
import { CustomerFilesResultsComponent } from './customer-files-results/customer-files-results.component';

const routes: Routes = [
    {
        path: 'basic-tables/customer-files/:customer_id', children: [
            {
                path: '', component: CustomerFilesComponent, children: [
                    { path: '', component: CustomerFilesListComponent },
                    { path: 'new', component: CustomerFilesUploadComponent },
                    { path: 'edit/:id', component: CustomerFilesFormComponent },
                    { path: 'results/:id', component: CustomerFilesResultsComponent },
                ]
            }
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class CustomerFilesRoutingModule {
}
