import { Component, OnInit } from '@angular/core';
import { CustomerFilesService } from '../services/customer-files.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService } from '../../../general/services/crud.service';
import { CustomerFileData } from '../../../models/basic-tables/customer-file-data';
import { CustomerFile } from '../../../models/basic-tables/customer-file';
import { AlertService } from '../../../plugins/services/alert.service';
import { BlockUiService } from '../../../plugins/services/block-ui.service';
import { DisposableBag } from '../../../plugins/components/disposable-bag/disposable-bag';

@Component({
    selector: 'app-customer-files-results',
    templateUrl: './customer-files-results.component.html',
    styleUrls: [ './customer-files-results.component.scss' ]
})
export class CustomerFilesResultsComponent extends DisposableBag implements OnInit {

    data: any[] = [];
    columns: string[] = [];
    customerFileData: CustomerFileData = {};
    customerFile: CustomerFile = {};
    filters = {};
    showFilter = false;
    fullButtonMode = false;
    errors = {};

    constructor(private customerFilesService: CustomerFilesService,
                private crudService: CrudService,
                private alertService: AlertService,
                private blockUiService: BlockUiService,
                private route: ActivatedRoute,
                private router: Router) {
        super();
    }

    ngOnInit(): void {
        let id = this.route.snapshot.params.id;

        let s1 = this.crudService.get('customer-file-data', id)
            .subscribe(result => {
                this.customerFileData = result;
                let s2 = this.crudService.get('customer-files', this.customerFileData.customer_file)
                    .subscribe(customerFile => {
                        this.customerFile = customerFile;
                        this.confirmRefreshData();
                    });
                this.add(s2);
            });

        this.add(s1);
    }

    removeLine(lineIndex: number) {
        this.alertService
            .confirmSimNao('Deseja REMOVER a linha?')
            .then(({ isConfirmed }) => isConfirmed ? this.confirmRemoveLine(lineIndex) : undefined);
    }

    confirmRemoveLine(lineIndex: number) {
        let index = this.data.findIndex(a => a.index == lineIndex);
        this.data.splice(index, 1);
    }

    moveRowUp(line: number) {
        if (line > 0) {
            let element = this.data[ line ];
            let previous = this.data[ line - 1 ];
            this.data[ line - 1 ] = element;
            this.data[ line ] = previous;
        }
    }

    moveRowDown(line: number) {
        if (line < this.data.length - 1) {
            let element = this.data[ line ];
            let previous = this.data[ line + 1 ];
            this.data[ line + 1 ] = element;
            this.data[ line ] = previous;
        }
    }

    addRowBefore(line: number) {
        let data1 = this.data.slice(0, line);
        let data2 = this.data.slice(line, this.data.length);

        this.data = data1.concat({}, data2);
    }

    addRowAfter(line: number) {
        this.addRowBefore(line + 1);
    }

    addColumnLeft(index: number) {
        let cols1 = this.columns.slice(0, index);
        let cols2 = this.columns.slice(index, this.data.length);

        this.columns = cols1.concat(`column_${ this.columns.length }`, cols2);
    }

    addColumnRight(index: number) {
        this.addColumnLeft(index + 1);
    }

    removeColumn(index: number) {
        this.alertService
            .confirmSimNao('Deseja EXCLUIR essa coluna? Esta ação não poderá ser desfeita!')
            .then(({ isConfirmed }) => isConfirmed ? this.confirmRemoveColumn(index) : undefined);
    }

    moveColumnRight(index: number) {
        if (index < this.columns.length - 1) {
            let original = this.columns[ index ];
            let destination = this.columns[ index + 1 ];
            this.columns[ index ] = destination;
            this.columns[ index + 1 ] = original;
        }
    }

    moveColumnLeft(index: number) {
        if (index > 0) {
            let original = this.columns[ index ];
            let destination = this.columns[ index - 1 ];
            this.columns[ index ] = destination;
            this.columns[ index - 1 ] = original;
        }
    }

    confirmRemoveColumn(index: number) {
        let attr = this.columns[ index ];
        this.columns.splice(index, 1);

        this.data.forEach(item => delete item[ attr ]);
    }

    refreshData() {
        this.alertService
            .confirmSimNao('Deseja ATUALIZAR os dados? Você irá perder as alterações realizadas!')
            .then(({ isConfirmed }) => isConfirmed ? this.confirmRefreshData() : undefined);
    }

    confirmRefreshData() {
        this.blockUiService.block('#table-data');
        let s1 = this.customerFilesService
            .getAnalysisData(this.customerFileData.id)
            .subscribe(data => {
                    this.data = data;

                    if (this.data.length > 0) {
                        this.columns = Object.keys(this.data[ 0 ]).filter(c => c !== 'index' && c !== 'is_editing');
                        this.columns.forEach(c => this.filters[ c ] = '');
                    }

                    this.rebuildIndexes();

                    this.blockUiService.unBlock('#table-data');
                }, () => this.blockUiService.unBlock('#table-data')
            );
        this.add(s1);
    }

    private rebuildIndexes() {
        this.data.forEach((a, index) => {
            a.index = index;
            delete a.is_editing;
        });
    }

    persistData() {
        this.alertService
            .confirmSimNao('Deseja PERSISTIR os dados?')
            .then(({ isConfirmed }) => isConfirmed ? this.confirmPersisData() : undefined);
    }

    confirmPersisData() {
        this.rebuildIndexes();
        let s1 = this.customerFilesService.updateData(this.customerFileData.id, this.data)
            .subscribe((result) => this.sucess(), errors => this.errors = errors);

        this.add(s1);
    }

    sucess(): void {
        this.alertService
            .success(`Registro alterado  com sucesso!`)
            .then(() => undefined);
    }

    goBack() {
        let customer_id = this.route.parent.parent.snapshot.params.customer_id;
        let navigateTo = this.route.parent.parent.routeConfig.path.replace(':customer_id', customer_id);
        this.router.navigate([ navigateTo ]);
    }
}
