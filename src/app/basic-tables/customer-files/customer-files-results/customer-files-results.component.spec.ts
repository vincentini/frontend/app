import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerFilesResultsComponent } from './customer-files-results.component';

describe('CustomerFilesResultsComponent', () => {
  let component: CustomerFilesResultsComponent;
  let fixture: ComponentFixture<CustomerFilesResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerFilesResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerFilesResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
