import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerFilesUploadComponent } from './customer-files-upload.component';

describe('CustomerFilesUploadComponent', () => {
  let component: CustomerFilesUploadComponent;
  let fixture: ComponentFixture<CustomerFilesUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerFilesUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerFilesUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
