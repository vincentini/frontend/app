import { AfterViewInit, Component, OnInit } from '@angular/core';
import { CustomerFile } from '../../../models/basic-tables/customer-file';
import { FileCategory } from '../../../models/basic-tables/file-category';
import { CrudService } from '../../../general/services/crud.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../plugins/services/alert.service';
import { S3Bucket } from '../../../models/basic-tables/s3-bucket';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { environment } from '../../../../environments/environment';
import { AuthService } from '../../../security/services/auth.service';
import { BlockUiService } from '../../../plugins/services/block-ui.service';

@Component({
    selector: 'app-customer-files-upload',
    templateUrl: './customer-files-upload.component.html',
    styleUrls: [ './customer-files-upload.component.scss' ]
})
export class CustomerFilesUploadComponent implements OnInit, AfterViewInit {

    customerFile: CustomerFile = {};
    fileCategories: FileCategory[] = [];
    s3Buckets: S3Bucket[] = [];

    errors = {
        s3_bucket: undefined
    };

    constructor(private crudService: CrudService,
                private router: Router,
                private route: ActivatedRoute,
                private alertService: AlertService,
                private authService: AuthService,
                private blockUiService: BlockUiService) {
    }

    ngOnInit(): void {
        this.customerFile.customer = this.route.parent.parent.snapshot.params.customer_id;

        this.crudService.list('s3-buckets', '', 1, 99999)
            .subscribe(s3Buckets => {
                this.s3Buckets = s3Buckets.results;
                if (this.s3Buckets.length > 0) {
                    this.setS3Bucket(this.s3Buckets[ 0 ]);
                }
            });
    }

    ngAfterViewInit() {
    }

    setS3Bucket(s3Bucket: S3Bucket) {
        this.customerFile.s3_bucket = s3Bucket.id;
    }

    buildConfig(): DropzoneConfigInterface {
        let config: DropzoneConfigInterface = {
            url: `${ environment.baseApiUrl }/api/customer-files-upload/?customer_id=${ this.customerFile.customer }&s3_bucket_id=${ this.customerFile.s3_bucket }`,
            maxFilesize: 100,
            acceptedFiles: 'application/pdf',
            headers: {
                'Authorization': 'Bearer ' + this.authService.getToken().access
            },
            autoReset: 5,
            errorReset: 5,
            addRemoveLinks: true,
            maxFiles: 10,
            parallelUploads: 10,
            dictFileTooBig: `Arquivo muito grande ({{filesize}}). Tamanho máximo de {{maxFilesize}}.`,
            dictMaxFilesExceeded: `Máximo de arquivos é de {{maxFiles}}`,
            method: 'PUT'
        };

        return config;
    }

    onUploadError($event) {
        this.alertService.error(`Erro ao adicionar arquivo: ${ $event[ 1 ].message || $event[ 1 ].detail }`);
    }

    onUploadSuccess($event) {
        if ($event[ 0 ].status == 'success') {
            let customerFile: CustomerFile = $event[ 1 ];
            this.alertService.success('Arquivo salvo com sucesso!')
                .then(() => this.goToFile(customerFile.id));
        }
    }

    goToFile(id: number) {
        let customer_id = this.route.parent.parent.snapshot.params.customer_id;
        let navigateTo = this.route.parent.parent.routeConfig.path.replace(':customer_id', customer_id);
        this.router.navigate([ navigateTo, 'edit', id ]);
    }

    cancel(): void {
        let customer_id = this.route.parent.parent.snapshot.params.customer_id;
        let navigateTo = this.route.parent.parent.routeConfig.path.replace(':customer_id', customer_id);
        this.router.navigate([ navigateTo ]);
    }
}
