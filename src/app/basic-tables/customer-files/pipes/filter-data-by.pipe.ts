import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterDataBy',
    pure: false
})
export class FilterDataByPipe implements PipeTransform {

    transform(data: any[], columns: string[], filters: any): any[] {
        let result = data;

        columns.forEach(column => {
            if (filters[ column ]) {
                result = data.filter(a => a[ column ]
                    .toString()
                    .toUpperCase()
                    .indexOf(filters[ column ].toString().toUpperCase()) >= 0);
            }
        });

        return result;
    }
}
