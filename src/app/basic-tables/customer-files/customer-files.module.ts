import { NgModule } from '@angular/core';

import { CustomerFilesRoutingModule } from './customer-files-routing.module';
import { CustomerFilesComponent } from './customer-files/customer-files.component';
import { CustomerFilesListComponent } from './customer-files-list/customer-files-list.component';
import { CustomerFilesFormComponent } from './customer-files-form/customer-files-form.component';
import { CustomerFilesUploadComponent } from './customer-files-upload/customer-files-upload.component';
import { PluginsModule } from '../../plugins/plugins.module';
import { TemplateModule } from '../../template/template.module';
import { CustomerFilesResultsComponent } from './customer-files-results/customer-files-results.component';
import { FilterDataByPipe } from './pipes/filter-data-by.pipe';


@NgModule({
    declarations: [
        CustomerFilesComponent,
        CustomerFilesListComponent,
        CustomerFilesFormComponent,
        CustomerFilesUploadComponent,
        CustomerFilesResultsComponent,
        FilterDataByPipe
    ],
    imports: [
        PluginsModule,
        TemplateModule,
        CustomerFilesRoutingModule,
    ]
})
export class CustomerFilesModule {
}
