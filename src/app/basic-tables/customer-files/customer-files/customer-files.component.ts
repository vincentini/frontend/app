import { Component, OnInit } from '@angular/core';
import { CrudService } from '../../../general/services/crud.service';
import { ActivatedRoute } from '@angular/router';
import { Customer } from '../../../models/basic-tables/customer';
import { AlertService } from '../../../plugins/services/alert.service';

@Component({
    selector: 'app-customer-files',
    templateUrl: './customer-files.component.html',
    styleUrls: [ './customer-files.component.scss' ]
})
export class CustomerFilesComponent implements OnInit {

    customer: Customer = {};

    constructor(private crudService: CrudService,
                private route: ActivatedRoute,
                private alertService: AlertService) {
    }

    ngOnInit(): void {
        const customer_id = this.route.snapshot.params.customer_id;

        this.crudService.get('customers', customer_id)
            .subscribe((customer) => this.customer = customer, (error) => {
                this.alertService.error(`Erro ao buscar cliente de id ${ customer_id }: ${ error.message }`)
                    .then(() => history.back());
            });
    }
}
