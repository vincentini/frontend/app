import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { CustomerFileData } from '../../../models/basic-tables/customer-file-data';
import { PageResult } from '../../../models/list/page-result.model';

@Injectable({
    providedIn: 'root'
})
export class CustomerFilesService {

    constructor(private httpClient: HttpClient) {
    }

    getS3Link(id: number): Observable<string> {
        return this.httpClient
            .get<string>(`${ environment.baseApiUrl }/api/customer-files/${ id }/s3-link`);
    }

    startAnalysisTable(id: number): Observable<CustomerFileData> {
        return this.httpClient
            .get<CustomerFileData>(`${ environment.baseApiUrl }/api/customer-files/${ id }/start-analysis-table/`);
    }

    // statusAnalysis(id: number): Observable<CustomerFileData> {
    //     return this.httpClient
    //         .get<CustomerFileData>(`${ environment.baseApiUrl }/api/customer-file-data/${ id }/`);
    // }

    // updateStatusAnalysis(id: number): Observable<CustomerFileData> {
    //     return this.httpClient
    //         .get<CustomerFileData>(`${ environment.baseApiUrl }/api/customer-file-data/${ id }/update-status/`);
    // }

    getAnalysisFromCustomerFile(id: number): Observable<PageResult<CustomerFileData>> {
        return this.httpClient
            .get(`${ environment.baseApiUrl }/api/customer-file-data/?customer_file=${ id }`);
    }

    getAnalysisData(id: number): Observable<any> {
        return this.httpClient
            .get<CustomerFileData>(`${ environment.baseApiUrl }/api/customer-file-data/${ id }/data/`);
    }

    updateData(id: number, data: any) {
        return this.httpClient
            .patch<CustomerFileData>(`${ environment.baseApiUrl }/api/customer-file-data/${ id }/update-data/`, { data });
    }
}
