import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerFilesFormComponent } from './customer-files-form.component';

describe('CustomerFilesFormComponent', () => {
  let component: CustomerFilesFormComponent;
  let fixture: ComponentFixture<CustomerFilesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerFilesFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerFilesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
