import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FileCategory } from '../../../models/basic-tables/file-category';
import { CrudService } from '../../../general/services/crud.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../plugins/services/alert.service';
import { take } from 'rxjs/operators';
import { CustomerFile } from '../../../models/basic-tables/customer-file';

@Component({
    selector: 'app-customer-files-form',
    templateUrl: './customer-files-form.component.html',
    styleUrls: [ './customer-files-form.component.scss' ]
})
export class CustomerFilesFormComponent implements OnInit {

    customerFile: CustomerFile = {};
    fileCategories: FileCategory[] = [];
    errors = {
        name: undefined,
        status: undefined
    };

    @ViewChild('nameField') nameField: ElementRef;

    constructor(private crudService: CrudService,
                private router: Router,
                private route: ActivatedRoute,
                private alertService: AlertService) {
    }

    ngOnInit(): void {
        let id = this.route.snapshot.params.id;
        if (id) {
            this.crudService
                .get<FileCategory>('customer-files', id)
                .subscribe(
                    customerFile => this.customerFile = customerFile,
                    errors => {
                        this.errors = errors;
                    });
        }

        this.crudService.list('file-categories', '', 1, 99999)
            .subscribe(fileCategories => this.fileCategories = fileCategories.results);
    }

    ngAfterViewInit(): void {
        this.nameField.nativeElement.focus();
    }

    save(): void {
        // @ts-ignore
        this.errors = {};

        let customerFileUpdate = new CustomerFile();
        customerFileUpdate.file_category = this.customerFile.file_category;
        customerFileUpdate.file_description = this.customerFile.file_description;

        this.crudService.update('customer-files', this.customerFile.id, customerFileUpdate)
            .subscribe(result => this.sucess('alterado'), errors => this.errors = errors);

    }

    sucess(acao: string): void {
        this.alertService
            .success(`Registro ${ acao }  com sucesso!`)
            .then(() => this.cancel());
    }

    cancel(): void {
        let customer_id = this.route.parent.parent.snapshot.params.customer_id;
        let navigateTo = this.route.parent.parent.routeConfig.path.replace(':customer_id', customer_id);
        this.router.navigate([ navigateTo ]);
    }
}
