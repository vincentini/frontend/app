import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileCategoriesListComponent } from './file-categories-list.component';

describe('FileCategoriesListComponent', () => {
  let component: FileCategoriesListComponent;
  let fixture: ComponentFixture<FileCategoriesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileCategoriesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileCategoriesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
