import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PageResult } from '../../../models/list/page-result.model';
import { CrudService } from '../../../general/services/crud.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../plugins/services/alert.service';
import { DataPaginationEvent } from '../../../plugins/components/data-table/model/data-pagination.event';
import { FileCategory } from '../../../models/basic-tables/file-category';

@Component({
    selector: 'app-file-categories-list',
    templateUrl: './file-categories-list.component.html',
    styleUrls: [ './file-categories-list.component.scss' ]
})
export class FileCategoriesListComponent implements OnInit {

    searchBy = '';
    pageSize = 5;
    currentPage = 1;
    data: PageResult<FileCategory> = {};
    @ViewChild('searchField') searchField: ElementRef;

    constructor(
        private crudService: CrudService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private alertService: AlertService) {
    }

    ngOnInit(): void {
        this.search();
    }

    ngAfterViewInit(): void {
        this.searchField.nativeElement.focus();
    }

    onPaginate(event: DataPaginationEvent): void {
        this.search(event.page);
    }

    search(pageNumber = 1): void {
        this.currentPage = pageNumber;
        this.crudService
            .list<FileCategory>('file-categories', this.searchBy, this.currentPage, this.pageSize)
            .subscribe(result => this.data = result);
    }

    newFileCategory(): void {
        this.router.navigate([ 'new' ], { relativeTo: this.activatedRoute });
    }

    editFileCategory(id: number): void {
        this.router.navigate([ 'edit', id ], { relativeTo: this.activatedRoute });
    }

    deleteFileCategory(id: number) {
        this.alertService.confirmSimNao('Confirma a EXCLUSÃO deste cliente?')
            .then((result) =>
                result.isConfirmed ? this.confirmDelete(id) : undefined);
    }

    confirmDelete(id: number): void {
        this.crudService.delete('file-categories', id)
            .subscribe(() => {
                this.search(1);
                this.alertService.info('Registro excluído com sucesso!');
            }, (error => {
                this.alertService.error('Ocorreu um erro ao excluir o registro!');
            }));
    }
}
