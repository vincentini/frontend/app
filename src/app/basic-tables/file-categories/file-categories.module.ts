import { NgModule } from '@angular/core';

import { FileCategoriesRoutingModule } from './file-categories-routing.module';
import { FileCategoriesComponent } from './file-categories/file-categories.component';
import { FileCategoriesFormComponent } from './file-categories-form/file-categories-form.component';
import { FileCategoriesListComponent } from './file-categories-list/file-categories-list.component';
import { PluginsModule } from '../../plugins/plugins.module';
import { TemplateModule } from '../../template/template.module';


@NgModule({
  declarations: [FileCategoriesComponent, FileCategoriesFormComponent, FileCategoriesListComponent],
  imports: [
    PluginsModule,
    FileCategoriesRoutingModule,
    TemplateModule
  ]
})
export class FileCategoriesModule { }
