import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileCategoriesFormComponent } from './file-categories-form.component';

describe('FileCategoriesFormComponent', () => {
  let component: FileCategoriesFormComponent;
  let fixture: ComponentFixture<FileCategoriesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileCategoriesFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileCategoriesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
