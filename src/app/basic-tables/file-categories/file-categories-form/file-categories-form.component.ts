import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FileCategory } from '../../../models/basic-tables/file-category';
import { CrudService } from '../../../general/services/crud.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../plugins/services/alert.service';
import { take } from 'rxjs/operators';

@Component({
    selector: 'app-file-categories-form',
    templateUrl: './file-categories-form.component.html',
    styleUrls: [ './file-categories-form.component.scss' ]
})
export class FileCategoriesFormComponent implements OnInit, AfterViewInit {

    fileCategory: FileCategory = {
        status: 'A'
    };
    errors = {
        name: undefined,
        status: undefined
    };

    @ViewChild('nameField') nameField: ElementRef;

    constructor(private crudService: CrudService,
                private router: Router,
                private route: ActivatedRoute,
                private alertService: AlertService) {
    }

    ngOnInit(): void {
        const url = this.route.snapshot.url.join('');

        if (url !== 'new') {
            const s1 = this.route.params.subscribe(params => {
                if (params.id) {
                    this.crudService
                        .get<FileCategory>('file-categories', params.id)
                        .subscribe(fileCategory => this.fileCategory = fileCategory, errors => this.errors = errors);
                }
            });
        }
    }

    ngAfterViewInit():void {
        this.nameField.nativeElement.focus();
    }

    save(): void {
        const url = this.route.snapshot.url.join('/');
        // @ts-ignore
        this.errors = {};

        if (url === 'new') {
            this.crudService.create('file-categories', this.fileCategory)
                .subscribe(result => this.sucess('incluído'), errors => this.errors = errors);
        } else if (url.startsWith('edit')) {
            this.crudService.update('file-categories', this.fileCategory.id, this.fileCategory)
                .subscribe(result => this.sucess('alterado'), errors => this.errors = errors);
        }
    }

    sucess(acao: string): void {
        this.alertService
            .success(`Registro ${ acao }  com sucesso!`)
            .then(() => this.cancel());
    }

    cancel(): void {
        this.router.navigate([ this.route.parent.parent.routeConfig.path ]);
    }
}
