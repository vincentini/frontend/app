import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileCategoriesComponent } from './file-categories/file-categories.component';
import { FileCategoriesListComponent } from './file-categories-list/file-categories-list.component';
import { FileCategoriesFormComponent } from './file-categories-form/file-categories-form.component';


const routes: Routes = [
    {
        path: 'basic-tables/file-categories', children: [
            {
                path: '', component: FileCategoriesComponent, children: [
                    { path: '', component: FileCategoriesListComponent },
                    { path: 'new', component: FileCategoriesFormComponent },
                    { path: 'edit/:id', component: FileCategoriesFormComponent },
                ]
            }
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class FileCategoriesRoutingModule {
}
