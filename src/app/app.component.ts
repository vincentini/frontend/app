import { AfterContentChecked, AfterViewInit, Component } from '@angular/core';
import { AuthService } from './security/services/auth.service';
import { Utils } from './template/utils/utils';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: [ './app.component.scss' ]
})
export class AppComponent implements AfterContentChecked, AfterViewInit {

    isAuthenticated = false;

    constructor(private authService: AuthService) {
        this.authService.startRefreshTimer();
    }

    ngAfterContentChecked(): void {
        this.isAuthenticated = this.authService.isAuthenticated();
    }

    ngAfterViewInit(): void {
        Utils.init();
    }
}
