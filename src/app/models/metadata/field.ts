export class Field {
    id?: number;
    field_name?: string;
    field_type?: number;
    field_description?: string;
    field_size?: number;
}

export class FieldType {
    id: number;
    name: string;
}

export const fieldTypes: FieldType[] = [
    { id: 1, name: 'Texto' },
    { id: 2, name: 'Inteiro' },
    { id: 3, name: 'Decimal' },
    { id: 4, name: 'Percentual' },
    { id: 5, name: 'Monetário' }
];
