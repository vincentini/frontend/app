import { Field } from './field';

export class Table {
    id?: number;
    table_name?: string;
    table_description?: string;
    table_fields?: Field[]
}
