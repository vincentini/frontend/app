export class PageResult<T> {
    count?: number;
    pages?: number;
    next?: string;
    previous?: string;
    results?: T[];
}
