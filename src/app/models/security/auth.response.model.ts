export class AuthResponseModel {
    access: string;
    refresh: string;
}
