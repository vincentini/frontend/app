export class Menu {
    name: string;
    icon: string;
    link?: string;
}

export class MenuGroup {
    name: string;
    icon?: string;
    menus: Menu[];
}
