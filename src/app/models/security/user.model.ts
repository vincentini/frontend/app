export class User {
    username?: string;
    firstName?: string;
    lastName?: string;
    name?: string;
    email?: string;
    photo?: string;
}
