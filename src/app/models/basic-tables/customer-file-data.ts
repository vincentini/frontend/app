export class CustomerFileData {
    id?: number;
    customer_file?: number;
    aws_job_id?: string;
    status?: string;
    job_last_refresh?: string;
    data?: string;
}
