export class CustomerFile {
    id?: number;
    file_category?: number;
    s3_bucket?: number;
    customer?: number;
    file_name?: string;

    file_extension?: string;
    s3_object_name?: string;
    file_description?: string;
    created_at?: string;

    s3_bucket_name?: string;
    file_category_name?: string;
    customer_file_data_id?: number;
    customer_file_data_status?: string;
}
