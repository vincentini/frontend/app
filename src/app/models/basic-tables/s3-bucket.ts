export class S3Bucket {
    id?: number;
    name?: string;
    bucket_name?: string;
    created?: boolean;
}
