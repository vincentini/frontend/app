import { NgModule } from '@angular/core';
import { SecurityRoutingModule } from './security-routing.module';
import { LoginComponent } from './components/login/login.component';
import { PluginsModule } from '../plugins/plugins.module';


@NgModule({
  declarations: [LoginComponent],
  imports: [
    PluginsModule,
    SecurityRoutingModule
  ]
})
export class SecurityModule { }
