import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

    constructor(private authService: AuthService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        if (!this.authService.isAuthenticated()) {
            this.router.navigate([ 'login' ]);
            return of(false);
        }

        return this.authService.refresh().pipe(
            map((result) => {
                if (!result) {
                    this.router.navigate([ 'login' ]);
                }
                return result;
            })
        );
    }
}
