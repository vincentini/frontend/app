import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MenuGroup } from '../../models/security/menu.model';

@Injectable({
    providedIn: 'root'
})
export class PermissionsService {

    constructor() {
    }

    getUserMenus(): Observable<MenuGroup[]> {
        const menuGroups: MenuGroup[] = [
            {
                name: 'Configurações',
                menus: [
                    {
                        name: 'Categorias de arquivo',
                        icon: 'fas fa-archive text-primary',
                        link: '/basic-tables/file-categories'
                    },
                    {
                        name: 'S3 Buckets',
                        icon: 'fab fa-bitbucket text-primary',
                        link: '/basic-tables/s3-buckets'
                    },
                ]
            },
            {
                name: 'Cadastros',
                icon: 'fab fa-wpforms', menus: [
                    {
                        name: 'Clientes',
                        icon: 'fas fa-users text-success',
                        link: '/basic-tables/customers'
                    }
                ]
            }, {
                name: 'Metadados',
                icon: '',
                menus: [
                    {
                        name: 'Tabelas',
                        icon: 'fa fa-table',
                        link: 'metadata/tables'
                    }
                ]
            }
        ];

        return of(menuGroups);
    }
}
