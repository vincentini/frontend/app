import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable, Subscription, throwError, timer } from 'rxjs';
import { AuthResponseModel } from '../../models/security/auth.response.model';
import { User } from '../../models/security/user.model';
import { Md5 } from 'ts-md5/dist/md5';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    tokenKey = 'jwt-token';

    private timerSubscription: Subscription;

    constructor(private httpClient: HttpClient) {
    }

    auth(username: string, password: string): Observable<any> {
        return this.httpClient
            .post<AuthResponseModel>(`${ environment.baseApiUrl }/api/token/`, { username, password })
            .pipe(map(result => this.saveLocalToken(result)));
    }

    refresh(): Observable<boolean> {
        this.startRefreshTimer();

        const token = this.getToken();

        if (token) {
            return this.httpClient
                .post<AuthResponseModel>(`${ environment.baseApiUrl }/api/token/refresh/`, { refresh: token.refresh })
                .pipe(
                    map(result => {
                        if (result && result.access) {
                            this.saveLocalToken({ refresh: token.refresh, access: result.access });
                            return true;
                        }
                        return false;
                    })
                );
        } else {
            return throwError({ message: 'Usuário não autenticado' });
        }
    }

    saveLocalToken(data: AuthResponseModel): void {
        localStorage.setItem(this.tokenKey, JSON.stringify((data)));
    }

    getToken(): AuthResponseModel {
        const token = localStorage.getItem('jwt-token');
        return token ? JSON.parse(token) : undefined;
    }

    isAuthenticated(): boolean {
        return this.getToken() !== undefined;
    }

    clearToken(): void {
        localStorage.removeItem(this.tokenKey);
        this.stopRefresh();
    }

    gravatarPhoto(user: User): string {
        return `https://www.gravatar.com/avatar/${ Md5.hashAsciiStr(user.email).toString() }`;
    }

    getUserInfo(): Observable<User> {
        return this.httpClient
            .get<User>(`${ environment.baseApiUrl }/api/me`)
            .pipe(
                map(user => {
                    user.photo = this.gravatarPhoto(user);
                    return user;
                })
            );
    }

    startRefreshTimer(): void {
        if (!this.timerSubscription) {
            this.timerSubscription = timer(2000, 60000)
                .subscribe(() => this.refresh()
                    .subscribe(result => !result ?? this.clearToken()));
        }
    }

    stopRefresh(): void {
        if (this.timerSubscription) {
            this.timerSubscription.unsubscribe();
            this.timerSubscription = undefined;
        }
    }
}
