import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

declare var $: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: [ './login.component.scss' ]
})
export class LoginComponent implements OnInit, AfterViewInit {
    username = '';
    password = '';
    errorMessage = '';

    @ViewChild('inputLogin') inputLogin: ElementRef;

    constructor(
        private authService: AuthService,
        private router: Router
    ) {
    }

    ngOnInit(): void {
        $('[data-toggle="tooltip"]').tooltip();
        $('.preloader').fadeOut();
        $('#to-recover').on('click', () => {
            $('#loginform').slideUp();
            $('#recoverform').fadeIn();
        });
        this.authService.clearToken();
    }

    ngAfterViewInit(): void {
        this.inputLogin.nativeElement.focus();
    }

    signIn(): void {
        this.errorMessage = '';
        if (this.formValidated()) {
            this.authService.auth(this.username, this.password)
                .subscribe(() => this.goHome(), (error) => this.loginFailed(error));
        } else {
            this.errorMessage = 'Informe o usuário e a senha!';
            this.inputLogin.nativeElement.focus();
        }
    }

    forgotPassword(): void {
        // TODO: Forgot password
        this.errorMessage = 'Função ainda não implementada!';
    }

    private formValidated(): boolean {
        return Boolean(this.username.trim() !== '' && this.password.trim() !== '');
    }

    private goHome(): void {
        this.router.navigate([ 'home' ]);
    }

    private loginFailed(error: any): void {
        console.error(error);
        this.errorMessage = 'Usuário/senha inválidos';
        this.inputLogin.nativeElement.focus();
    }
}
