import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DebounceDirective } from './directives/debounce.directive';
import { ButtonsRightComponent, DataTableComponent } from './components/data-table/data-table.component';
import { ColumnComponent } from './components/data-table/column/column.component';
import { PaginatorComponent } from './components/data-table/paginator/paginator.component';
import { ColumnTemplateComponent } from './components/data-table/column-template/column-template.component';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { FormatDatePipe } from './pipes/format-date.pipe';
import { BsModalRef, BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    declarations: [
        DebounceDirective,
        DataTableComponent,
        ColumnComponent,
        ColumnTemplateComponent,
        PaginatorComponent,
        ButtonsRightComponent,
        FormatDatePipe
    ],
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        DropzoneModule,
        ModalModule.forChild(),
        TooltipModule.forRoot(),
        BsDropdownModule.forRoot(),
        BrowserAnimationsModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        ModalModule,
        TooltipModule,
        BrowserAnimationsModule,
        BsDropdownModule,
        DebounceDirective,
        DataTableComponent,
        ColumnComponent,
        ColumnTemplateComponent,
        PaginatorComponent,
        ButtonsRightComponent,
        DropzoneModule,
        FormatDatePipe,
    ],
    providers: [
        BsModalService,
        BsModalRef
    ]
})
export class PluginsModule {
}
