import { Directive, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[ngModel][debounce]',
})
export class DebounceDirective implements OnInit {

    // tslint:disable-next-line:no-output-on-prefix
    @Output() public onDebounce = new EventEmitter<any>();

    // tslint:disable-next-line:no-input-rename
    @Input('debounce') public debounceTime = 500;

    private isFirstChange = true;

    constructor(public model: NgControl) {
    }

    ngOnInit(): void {
        this.model.valueChanges
            .pipe(
                debounceTime(this.debounceTime),
                distinctUntilChanged()
            )
            .subscribe(modelValue => {
                if (this.isFirstChange) {
                    this.isFirstChange = false;
                } else {
                    this.onDebounce.emit(modelValue);
                }
            });
    }
}
