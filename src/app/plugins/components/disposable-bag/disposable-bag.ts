import { Subscription } from 'rxjs';
import { Component, OnDestroy } from '@angular/core';

@Component({
    template: ''
})
export abstract class DisposableBag implements OnDestroy {

    subscriptions: Subscription[] = [];

    add(s: Subscription) {
        this.subscriptions.push(s);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }
}
