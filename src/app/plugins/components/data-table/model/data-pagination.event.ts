export class DataPaginationEvent {
    page?: number;
    pageSize?: number;
    idTable?: string;
}
